﻿using System.Collections;
using System.Collections.Generic;
using Rewired;
using UnityEngine;

public class BeatTester : MonoBehaviour  {
    
    // The Rewired player id of this character
    public int playerId = 0;
    private Player player; // The Rewired Player
    private bool beatTestButtonPressed;

    private void Awake() {
        player = ReInput.players.GetPlayer(playerId);
    }
    
    void Update() {
        GetInput();
        ProcessInput();
    }

    private void GetInput() {
        beatTestButtonPressed = player.GetButtonDown("Beat Test");
    }

    private void ProcessInput() {

        if (beatTestButtonPressed) {
            BeatManager.TryBeatAction(true);
        }

        beatTestButtonPressed = false;

    }
    
}
