﻿using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ResetManager : MonoBehaviour {

    public static ResetManager Instance;

    private FadeoutController fadeoutController;

    private bool hasDied;
    private float timeOfDeath;
    public float TimeUntilReset;

    private void Awake() {
        Instance = this;
    }

    public void RegisterFadeoutController(FadeoutController newFadeoutController) {
        fadeoutController = newFadeoutController;
    }

    public void RegisterDeath() {
        hasDied = true;
        timeOfDeath = TimingController.GameTime;
        CameraController.Instance.SetMusicTargetVolume(0f);
        if (fadeoutController != null) {
            fadeoutController.RegisterDeath();
        }
    }

    private void Update() {
        if (hasDied && TimingController.GameTime - timeOfDeath >= TimeUntilReset) {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            SceneManager.sceneLoaded -= OnSceneLoaded;
            SceneManager.sceneLoaded += OnSceneLoaded;
        }
    }

    private void OnSceneLoaded(Scene scene, LoadSceneMode mode) {
        TimingController.TimeOfLastReset = Time.time;
        TimingController.UnscaledTimeOfLastReset = Time.unscaledTime;
    }
    
    [Button("TestDeath")]
    public void TestDeath() {
        RegisterDeath();
    }

}
