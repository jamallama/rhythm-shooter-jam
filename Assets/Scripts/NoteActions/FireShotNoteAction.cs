﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Create a new Shot instance in the scene
/// </summary>
public class FireShotNoteAction : NoteAction {
    private PatternController controller;
    private Shot shotPrefab;
    private readonly List<ConfigurationEvent> shotInstances;

    public FireShotNoteAction(int index, float triggerTime, List<ConfigurationEvent> shotInstances, Shot shotPrefab, PatternController controller) : base(index, triggerTime) {
        this.controller = controller;
        this.shotPrefab = shotPrefab;
        this.shotInstances = shotInstances;
    }
    
    /// <summary>
    /// Shoot your shot.Shot, my dude
    /// </summary>
    public override void PerformAction() {
        Transform spawner = controller.GetSpawner();
        if (spawner == null) return;
        Enemy enemy = spawner.GetComponent<Enemy>();
        if (!spawner)    // Has the enemy been deleted?
            return;
        Shot shotInstance = Object.Instantiate(shotPrefab, spawner.position, enemy.RotatorTransform.rotation, EnemyManager.Instance.transform);
        shotInstances.Add(shotInstance);
    }
}
