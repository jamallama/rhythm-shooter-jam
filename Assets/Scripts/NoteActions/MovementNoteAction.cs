﻿using System;
using UnityEngine;

/// <summary>
/// Move the thing. 
/// </summary>
public class MovementNoteAction : NoteAction {
    private MovementEvent movementEvent;
    private Vector2 destination;
    private Action<Vector2> onMovement;
    
    // TODO: We aren't using the shotIndex value here, and it is also probably wrong.
    public MovementNoteAction(int index, float triggerTime, MovementEvent movementEvent, Action<Vector2> onMovement) : base(index, triggerTime) {
        this.movementEvent = movementEvent;
        this.onMovement = onMovement;
    }
    public override void PerformAction() {
        onMovement.Invoke(movementEvent.Destination);
    }
}