﻿using System;
using UnityEngine;
using Object = UnityEngine.Object;

/// <summary>
/// Spawn an enemy that can fire shots and stuff
/// </summary>
public class SpawnNoteAction : NoteAction {
    private Vector2 spawnLocation;
    private Quaternion spawnRotation;
    private Enemy entityPrefab;
    private Action<Enemy> onEntitySpawned;
    
    public SpawnNoteAction(int index, float triggerTime, Enemy entityPrefab, Vector2 spawnLocation, Quaternion spawnRotation, Action<Enemy> onEntitySpawned) : base(index, triggerTime) {
        this.spawnLocation = spawnLocation;
        this.spawnRotation = spawnRotation;
        this.entityPrefab = entityPrefab;
        this.onEntitySpawned = onEntitySpawned;
    }
    
    public override void PerformAction() {
        // TODO: Have enemies spawn somewhere more sensible, like in some sort of bucket referenced by an enemy controller
        Transform spawner = Object.FindObjectOfType<EnemyManager>().transform;
        Enemy entityInstance = Object.Instantiate(entityPrefab, spawnLocation, spawnRotation, spawner);
        onEntitySpawned.Invoke(entityInstance);
    }
}
