﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadeoutController : MonoBehaviour {

    public Image Image;

    public AnimationCurve OpacityByTimeSinceStart;
    public AnimationCurve OpacityByTimeSinceDeath;

    private bool hasDied;
    private float timeOfDeath;

    private void Start() {
        if (ResetManager.Instance != null) {
            ResetManager.Instance.RegisterFadeoutController(this);
        }
    }

    public void RegisterDeath() {
        hasDied = true;
        timeOfDeath = TimingController.GameTime;
    }

    private void Update() {
        if (!hasDied) {
            Image.color = new Color(0f, 0f, 0f, OpacityByTimeSinceStart.Evaluate(TimingController.GameTime));
        }
        else {
            Image.color = new Color(0f, 0f, 0f, OpacityByTimeSinceDeath.Evaluate(TimingController.GameTime - timeOfDeath));
        }
    }

}
