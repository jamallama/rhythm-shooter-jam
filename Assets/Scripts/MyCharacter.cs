﻿// MyCharacter.cs - A simple example showing how to get input from Rewired.Player
using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Rewired;
using Sirenix.OdinInspector;
using UnityEngine.Serialization;
using Object = UnityEngine.Object;

public class MyCharacter : MonoBehaviour {

    public static MyCharacter Instance;
    
    // The Rewired player id of this character
    public int playerId = 0;
    
    // The movement speed of this character
    public float moveSpeed = 5.0f;
    
    // The bullet speed
    public float bulletSpeed = 15.0f;
    [FormerlySerializedAs("dashDuration")] public float dabOnTheHatersDuration = 0.25f;
    [FormerlySerializedAs("DashDistance")] public float DabOnTheHatersDistance = 5.0f;
    public AnimationCurve DashCurve;
    
    // Assign a prefab to this in the inspector.
    public BulletParticleSystem bulletPrefab;
    // This is the parent object that all of the spawned bullets will be attached to. 
    private Transform bulletBucket;

    private Player player; // The Rewired Player
    private CharacterController cc;
    private Vector3 moveVector;
    private Vector3 shootVector;
    [HideInInspector] public Vector2 lastMeaningfulShootVector = new Vector2(0f, 0.2f);
    private bool fireButtonPressed;
    private bool dabOnTheHatersButtonPressed;
    private bool dabOnTheHatersIsActive = false;
    private float timeDabOnTheHatersStarted = 0f;
    private Vector2 dabOnTheHatersStartPoint;
    private Vector2 dabOnTheHatersEndPoint;

    private float screenMinX, screenMaxX ,screenMinY ,screenMaxY;
    private float playerRadius;

    public GameObject ShotFXPrefab;
    public List<FxController> ShotResizeFX;
    [FormerlySerializedAs("DashResizeFX")] public FxController DabOnTheHatersResizeFX;
    [FormerlySerializedAs("DashTrail")] public ParticleSystem DabOnTheHatersTrail;
    public FxController BeatFailureFX;
    public GameObject DeathFXPrefab;
    
    public Collider2D Hitbox;

    [Header("Aim Indicator")]
    public Transform AimArrowPivot;

    [Header("Stun")]
    public float StunDuration;
    private float stunEndTime;

    void Awake() {
        Instance = this; // lol
        // Get the Rewired Player object for this player and keep it for the duration of the character's lifetime
        player = ReInput.players.GetPlayer(playerId);
        // Get the character controller
        cc = GetComponent<CharacterController>();
        // If you want the min max values to update if the resolution changes 
        // set them in update else set them in Start
        float camDistance = Vector3.Distance(transform.position, Camera.main.transform.position);
        Vector2 bottomCorner = Camera.main.ViewportToWorldPoint(new Vector3(0,0, camDistance));
        Vector2 topCorner = Camera.main.ViewportToWorldPoint(new Vector3(1,1, camDistance));

        playerRadius = cc.radius / 4;
        
        screenMinX = bottomCorner.x + playerRadius;
        screenMaxX = topCorner.x - playerRadius;
        screenMinY = bottomCorner.y + playerRadius;
        screenMaxY = topCorner.y - playerRadius;

        Debug.Log("Player Awake");
    }

    void Start() {
        bulletBucket = GameObject.FindGameObjectWithTag("PlayerBulletBucket").transform;
    }

    void Update() {
        GetInput();
        ProcessInput();

        // Get current position
         Vector3 pos = transform.position;
 
         // Horizontal constraint
         if(pos.x < screenMinX) pos.x = screenMinX;
         if(pos.x > screenMaxX) pos.x = screenMaxX;
 
         // vertical constraint
         if(pos.y < screenMinY) pos.y = screenMinY;
         if(pos.y > screenMaxY) pos.y = screenMaxY;
 
         // Update position
         transform.position = pos;

         ProcessDash();

    }

    /// <summary>
    /// When a bullet hits your hitbox and you're like "oh no"
    /// </summary>
    [Button("Administer The Hurt")]
    public void AdministerTheHurt() {
        Instantiate(DeathFXPrefab, transform.position, Quaternion.identity, null);
        ResetManager.Instance.RegisterDeath();
        Destroy(gameObject);
    }

    private void GetInput() {
        // Get the input from the Rewired Player. All controllers that the Player owns will contribute, so it doesn't matter
        // whether the input is coming from a joystick, the keyboard, mouse, or a custom controller.

        moveVector.x = player.GetAxis("Move Horizontal"); // get input by name or action id
        moveVector.y = player.GetAxis("Move Vertical");
        shootVector.x = player.GetAxis("Shoot Stick Horizontal"); // get input by name or action id
        shootVector.y = player.GetAxis("Shoot Stick Vertical");
        if (shootVector.magnitude > 0.2f) {
            lastMeaningfulShootVector = shootVector;
        }
        fireButtonPressed = player.GetButtonDown("Fire");
        dabOnTheHatersButtonPressed = player.GetButtonDown("Dash");
    }

    private void ProcessInput() {

        if (Time.time < stunEndTime) {
            fireButtonPressed = dabOnTheHatersButtonPressed = false;
            return;
        }
        
        // Process movement
        if (moveVector.x != 0.0f || moveVector.y != 0.0f) {
            if (dabOnTheHatersIsActive) {
                transform.position = Vector2.Lerp(dabOnTheHatersStartPoint, dabOnTheHatersEndPoint, DashCurve.Evaluate((Time.time - timeDabOnTheHatersStarted) / dabOnTheHatersDuration));
            } else {
                cc.Move(moveVector * moveSpeed * Time.deltaTime);
            }
        }

        // Process fire
        if (fireButtonPressed) {
            checkBeatAndInvoke(shoot);
        }
        
        // Process dash
        if (dabOnTheHatersButtonPressed) {
            checkBeatAndInvoke(tryDash);
        }
        
        // Do Aim Indication
        AimArrowPivot.rotation = Quaternion.identity;
        AimArrowPivot.Rotate(0f, 0f, MathUtilities.RotationToLookAtPoint(Vector2.right, lastMeaningfulShootVector));

    }

    // Checks if the action was on-beat, and if so -- invokes the function
    private void checkBeatAndInvoke(Action func1) { 
        BeatManager.TryBeatAction(true);
        if (BeatManager.MostRecentActionSuccess) {
            func1.Invoke();
        }
        else {
            stunEndTime = Time.time + StunDuration;
            BeatFailureFX.TriggerAll(1f);
        }
    }

    // Checks if the dash is available, before trying to dash
    private void tryDash() {
        if (dabOnTheHatersIsActive == false) {
            dabOnTheHatersIsActive = true;
            dabOnTheHatersStartPoint = transform.position;
            dabOnTheHatersEndPoint = dabOnTheHatersStartPoint + (Vector2)moveVector.normalized * DabOnTheHatersDistance;
            DabOnTheHatersResizeFX.TriggerAll(1f);
            timeDabOnTheHatersStarted = Time.time;
            cc.detectCollisions = false;
            Hitbox.gameObject.SetActive(false);
            var dashTrailEmission = DabOnTheHatersTrail.emission;
            dashTrailEmission.rateOverDistance = 0.75f;
        }
    }

    private void shoot() {
        BulletParticleSystem bSystem = Instantiate(bulletPrefab, transform.position, Quaternion.identity, bulletBucket);
        bSystem.transform.Rotate(0f, 0f, MathUtilities.RotationToLookAtPoint(Vector2.right, lastMeaningfulShootVector));
        bSystem.Shoot();
        GameObject fx = Instantiate(ShotFXPrefab, transform.position, Quaternion.identity, null);
        fx.transform.Rotate(0f, 0f, MathUtilities.RotationToLookAtPoint(Vector2.right, lastMeaningfulShootVector));
        ShotResizeFX.ForEach(e => e.TriggerAll(1f));
    }

    private void ProcessDash() {
        if (!dabOnTheHatersIsActive) return;
        if (Time.time - timeDabOnTheHatersStarted > dabOnTheHatersDuration) {
            dabOnTheHatersIsActive = false;
            var dashTrailEmission = DabOnTheHatersTrail.emission;
            dashTrailEmission.rateOverDistance = 0;
            cc.detectCollisions = true;
            Hitbox.gameObject.SetActive(true);
        }
    }

    
}