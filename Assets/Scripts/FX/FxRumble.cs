﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Rewired;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class FxRumble : FxVariableValue {

    public bool ApplyToParentEntity;
    
    public override void SetUp (GameObject newAnchor = null) {

        base.SetUp(newAnchor);
        if (newAnchor == null) return;
        Anchor = newAnchor;

    }
    
    public override void Trigger (GameObject newAnchor = null, float power = 1f) {
        base.Trigger(newAnchor);
        AddFactorToManager(power);
    }
    
    public override void Toggle (bool toggleState, GameObject newAnchor = null, float power = 1f) {
        base.Toggle(toggleState, newAnchor);
        if (!toggleState) {
            RumbleManager.RemoveFactor(SfxId);
            return;
        }
        AddFactorToManager(power);
    }

    private void AddFactorToManager(float power) {
        RumbleManager.AddFactor(new RumbleValueFactor {
            ValueFactor = MyValueFactor(power, SfxId)
        });
    }
    
}

#if UNITY_EDITOR
[CanEditMultipleObjects]
[CustomEditor(typeof(FxRumble))]
public class FxRumbleEditor : Editor {
    override public void OnInspectorGUI() {
        
        var myScript = target as FxRumble;
        
        List<string> excludedProperties = new List<string>();
        serializedObject.Update();

        if(myScript.Style != ValueFactor.FactorStyle.Timed){
            excludedProperties.Add("Timed_Duration");
        }
        
        if(myScript.Style != ValueFactor.FactorStyle.Decay){
            excludedProperties.Add("Decay_DecayRate");
            excludedProperties.Add("Decay_MaxDuration");
        }
        
        if(myScript.Style != ValueFactor.FactorStyle.Curve){
            excludedProperties.Add("Curve_Curve");
        }
        else {
            excludedProperties.Add("BaseValue");
        }
        
        if(myScript.Style != ValueFactor.FactorStyle.Toggle){
            excludedProperties.Add("Toggle_Easing");
        }

        if (myScript.Style != ValueFactor.FactorStyle.Toggle || !myScript.Toggle_Easing) {
            excludedProperties.Add("Toggle_EaseIn");
            excludedProperties.Add("Toggle_EaseOut");
        }

        DrawPropertiesExcluding(serializedObject, excludedProperties.ToArray());
        serializedObject.ApplyModifiedProperties();

    }
}
#endif

public class RumbleValueFactor {

    public ValueFactor ValueFactor;
    public int ID => ValueFactor.ID;

}