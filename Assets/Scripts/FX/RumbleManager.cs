﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Rewired;

public class RumbleManager : MonoBehaviour {

	public int RewiredPlayerNum = 0;
	
    public static GameObject rumbleManagerObject;
	public static RumbleManager Instance;

	[Range (0.01F, 10F)] public float decayConstant = 2F;
    protected float tickTime = 0.1F;
    protected float tickTimer = 0F;
    protected bool rumbleNulledForPause = false;

    // Legacy — Rumble Spikes
	private List<RumbleSpike> spikes = new List<RumbleSpike>();
	
	// New — Value Factors
	private List<RumbleValueFactor> factors = new List<RumbleValueFactor>();

	// Use this for initialization
	void Awake () {

        if (rumbleManagerObject == null) {
            rumbleManagerObject = this.gameObject;
        }
        if (Instance == null) {
            Instance = this;
        }
        else if (Instance != this) {
            Destroy(gameObject);
        }

    }
	
	#region UpdateFunctions
	
	// Update is called once per frame
	void Update () {

        tickTimer += Time.deltaTime; 
        if (tickTimer > tickTime) { Tick(); }
		DecayRumble ();
		AgeValueFactors();

        //Nullify Rumble For Pause
        if ( Time.deltaTime == 0F && !rumbleNulledForPause ) {
            rumbleNulledForPause = true;
            if (ReInput.controllers.GetController(ControllerType.Joystick, RewiredPlayerNum) != null) {
	            //Debug.Log (player.playerNumber + " shake value: " + player.rumbleAmount);
	            ReInput.controllers.GetJoystick(RewiredPlayerNum).SetVibration(0, 0F);
	            ReInput.controllers.GetJoystick(RewiredPlayerNum).SetVibration(1, 0F);
            }
        }
        if (Time.deltaTime > 0F && rumbleNulledForPause) {
            rumbleNulledForPause = false;
        }

	}

    void Tick () {

        tickTimer = 0F;
        ApplyRumble();

    }

	private void DecayRumble () {
		List<RumbleSpike> spikesToRemove = new List<RumbleSpike>();
		foreach (RumbleSpike rs in spikes){
			if (rs.value > 0.01F) {
				rs.value *= (1F - rs.decayConstant * Time.deltaTime);
			}
			else {spikesToRemove.Add (rs);}
		}
		foreach (RumbleSpike rs in spikesToRemove){
			spikes.Remove (rs);
		}
	}

	private void AgeValueFactors() {
		for (int i = factors.Count - 1; i >= 0; i--) {
			factors[i].ValueFactor.UpdateAge();
			if (factors[i].ValueFactor.ShouldBeDeleted) factors.RemoveAt(i);
		}
	}
	
	private void ApplyRumble () {

		float rumbleAmountLeft = 0F;
		float rumbleAmountRight = 0F;

		// Sum Rumble Spikes
		foreach (RumbleSpike rs in spikes) {
			float leftValue = rs.value;
			float rightValue = rs.value;
			if (rs.panLevel >= 0F) {
				leftValue *= (1F - Mathf.Abs (rs.panLevel));
			}
			else {
				rightValue *= (1F - Mathf.Abs (rs.panLevel));
			}

			rumbleAmountLeft += leftValue * (1f - rumbleAmountLeft);
			rumbleAmountLeft = Mathf.Clamp (rumbleAmountLeft, 0f, 1f);
            if (rumbleAmountLeft < 0.01F) { rumbleAmountLeft = 0F; }

			rumbleAmountRight += rightValue * (1f - rumbleAmountRight);
			rumbleAmountRight = Mathf.Clamp (rumbleAmountRight, 0f, 1f);
            if (rumbleAmountRight < 0.01F) { rumbleAmountRight = 0F; }
		}
		
		// Calculate the greatest ValueFactor
		float valueFactorMax = 0f;
		List<RumbleValueFactor> valueFactors = factors.ToList();
		if (valueFactors.Count > 0) {
			valueFactorMax = valueFactors.Max(e => e.ValueFactor.Value);
		}

		// If ValueFactors have higher numbers than RumbleSpikes, use those.
		rumbleAmountLeft = Mathf.Max(rumbleAmountLeft, valueFactorMax);
		rumbleAmountRight = Mathf.Max(rumbleAmountRight, valueFactorMax);

        //Nullify Rumble While Paused
		//if (Time.deltaTime < 0.001F) {rumbleAmountRight = 0F; rumbleAmountLeft = 0F;}

        if (ReInput.controllers.GetController(ControllerType.Joystick, RewiredPlayerNum) != null) {
            //Debug.Log (player.playerNumber + " shake value: " + player.rumbleAmount);
            ReInput.controllers.GetJoystick(RewiredPlayerNum).SetVibration(0, rumbleAmountLeft);
            ReInput.controllers.GetJoystick(RewiredPlayerNum).SetVibration(1, rumbleAmountRight); 
        }

	}
	
	#endregion

	#region PublicFunctions
	
	public static void AddFactor(RumbleValueFactor newFactor) {
		if (newFactor.ID != -1) {
			if (Instance.GetFactorWithID(newFactor.ID) != null) {
				return;
			}
		}
		Instance.factors.Add(newFactor);
	}

	public static void RemoveFactor(int ID) {
		RumbleValueFactor factor = Instance.GetFactorWithID(ID);
		if (factor != null) {
			Instance.factors.Remove(factor);
		}
	}

	#endregion
	
	private RumbleValueFactor GetFactorWithID(int ID) {
		foreach (RumbleValueFactor factor in factors) {
			if (factor.ID == ID) return factor;
		}

		return null;
	}

}

[System.Serializable]
public class RumbleSpike {

	public float value;
	public float decayConstant;
	public bool allPlayers;
	[HideInInspector] public float panLevel;
	[HideInInspector] public Vector2 location;

	public RumbleSpike (float startValue, float newDecayConstant, Vector2 location) {
		value = startValue;
		decayConstant = newDecayConstant;
		value = Mathf.Clamp(value, 0F, 1F);
	}

	public RumbleSpike (float startValue, float newDecayConstant, bool affectsAllPlayers, Vector2 location) {
		value = startValue;
		decayConstant = newDecayConstant;
		allPlayers = affectsAllPlayers;
		value = Mathf.Clamp(value, 0F, 1F);
	}

}