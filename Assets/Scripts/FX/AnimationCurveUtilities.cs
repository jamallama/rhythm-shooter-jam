﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class AnimationCurveUtilities {

    public static AnimationCurve MultipliedCurve(AnimationCurve inputCurve, float amplitude) {
        if (amplitude == 1f) return inputCurve;
        Keyframe[] frames = new Keyframe[inputCurve.keys.Length];
        for (int i = 0; i < inputCurve.keys.Length; i++) {
            frames[i] = new Keyframe(inputCurve.keys[i].time, inputCurve.keys[i].value * amplitude, inputCurve.keys[i].inTangent * amplitude, inputCurve.keys[i].outTangent * amplitude);
        }
        return new AnimationCurve(frames);
    }

    public static void CopyCurve(this AnimationCurve curve, AnimationCurve other) {
        curve.keys = new Keyframe[0];
        for (int i = 0; i < other.keys.Length; i++) {
            Keyframe kf = new Keyframe();
            Keyframe otherKey = other.keys[i];
            kf.time = otherKey.time;
            kf.value = otherKey.value;
            kf.inWeight = otherKey.inWeight;
            kf.outWeight = otherKey.outWeight;
            kf.inTangent = otherKey.inTangent;
            kf.outTangent = otherKey.outTangent;
            kf.weightedMode = otherKey.weightedMode;
            curve.AddKey(kf);
            //AnimationUtility.SetKeyLeftTangentMode(curve, i, AnimationUtility.GetKeyLeftTangentMode(other, i));
            //AnimationUtility.SetKeyRightTangentMode(curve, i, AnimationUtility.GetKeyRightTangentMode(other, i));
        }
        curve.preWrapMode = other.preWrapMode;
        curve.postWrapMode = other.postWrapMode;
    }
    
    public static Keyframe LastFrame(this AnimationCurve curve) {
        return curve.keys[curve.keys.Length - 1];
    }

    public static Keyframe HighFrame(this AnimationCurve curve) {
        return curve.keys.OrderBy(e => e.value).Last();
    }

    public static Keyframe LowFrame(this AnimationCurve curve) {
        return curve.keys.OrderBy(e => e.value).First();
    }
    
}
