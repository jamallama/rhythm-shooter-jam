﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FxCamshake : Fx {
    
    [Range(0f, 1f)] public float Value = 1f;
    
    public override void Trigger (GameObject newAnchor = null, float power = 1f) {
        base.Trigger(newAnchor);
        CameraController.Instance.ApplyShake(Value);
    }

    private void Update() {
        if (ToggleState) {
            CameraController.Instance.ApplyShake(Value * lastPower);
        }
    }
    
}