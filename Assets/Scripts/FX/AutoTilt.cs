﻿ using System.Collections.Generic;
 using UnityEngine;

public class AutoTilt : MonoBehaviour {

	//TILT AMOUNT
    [Range(-10F, 10F)] public float MoveTilt;
	[Range(-5F, 5F)] public float XTiltMultiplier = 1F;
    public float TiltCap = 999999F;

    //SMOOTHING
    [Range (0F, 1F)] public float LastPositionSmoothing = .6F;
    [Range (0F, 1F)] public float TiltSmoothing = .9F;
    public bool IsFacingRight;
    public bool ForgetFirstFrame;
    private bool firstFrameForgotten;
    private Vector2 lastPosition;
    private readonly List<TiltFrame> lastTilts = new List<TiltFrame>();

    //SPECIAL FEATURES
    // ReSharper disable once NotAccessedField.Global — Accessed by Unity Inspector
    public bool ReverseWhenMovingRight;

    //TILT OFFSET
    public float TiltOffset;
    
    private float rootTilt;

    private Transform myTransform;

    private void Start() {
	    myTransform = transform;
        rootTilt = myTransform.localRotation.eulerAngles.z;
    }

    private void OnEnable() {
        firstFrameForgotten = false;
        lastTilts.Clear();
    }

    public void ClearTilts() {
        firstFrameForgotten = false;
        lastTilts.Clear();
    }

    private void Update () {
        if (ForgetFirstFrame && !firstFrameForgotten) {
            firstFrameForgotten = true;
            lastPosition = myTransform.position;
        }
		SetRotation ();
	}

    private void SetRotation() {
	    float tilt = 0F;
	    Vector3 position = myTransform.position;
	    float xDist = (position.x - lastPosition.x) / Time.unscaledDeltaTime;
	    float yDist = (position.y - lastPosition.y) / Time.unscaledDeltaTime;
	    float m = MoveTilt;
	    float xt = XTiltMultiplier;

	    //Apply Y Movement Tilt
	    tilt += -yDist * m;

	    //Apply X MovementTilt
	    tilt += -xDist * m * xt;

	    //Adjust list of prior tilts
	    lastTilts.Add(new TiltFrame(tilt, Time.deltaTime));

	    //Return actual tilt for this frame
	    float avg = 0;
	    float finalTilt = 0F;
	    float decayAmount;
	    float pow = 1f;
	    for (int i = 0; i < lastTilts.Count; i++) {
		    avg += 1F * lastTilts[i].FrameDuration;
		    finalTilt += lastTilts[i].Tilt * lastTilts[i].FrameDuration;
		    decayAmount = Mathf.Pow(TiltSmoothing, 60f * lastTilts[i].FrameDuration);
		    avg *= decayAmount;
		    finalTilt *= decayAmount;
		    pow *= decayAmount;
	    }

	    if (pow <= 0.001f) {
		    lastTilts.RemoveAt(0);
	    }

	    finalTilt /= avg;
	    finalTilt = Mathf.Clamp(finalTilt, -TiltCap, TiltCap);

	    //Apply tilt to transform
	    myTransform.localRotation = Quaternion.identity;
	    if (IsFacingRight) { 
		    finalTilt *= -1F;
	    }

	    float tiltToApply = rootTilt + TiltOffset + finalTilt;

	    if(tiltToApply > 0.001f || tiltToApply < 0.001f)
			myTransform.Rotate(0F, 0F, tiltToApply);

	    //Update last position
	    lastPosition = position;
    }

}

public struct TiltFrame {

    public readonly float Tilt;
    public readonly float FrameDuration;

    public TiltFrame(float tilt, float frameDuration) {
        Tilt = tilt;
        FrameDuration = frameDuration;
    }

}
