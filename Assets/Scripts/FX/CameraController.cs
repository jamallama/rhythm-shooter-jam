﻿using Sirenix.OdinInspector;
using UnityEngine;

public class CameraController : MonoBehaviour {

    public static CameraController Instance;
    public Camera Camera;

    public FxApplierTranslate ShakeTranslator;
    public FxApplierRotate ShakeRotator;
    public float ShakeTranslateMax;
    public float ShakeRotateMax;
    public float ShakeDecayRate;
    public float ShakePerlinSpeed;

    public AudioSource Music;

    public float TestShakeAmount = 1f;
    private float shakeTrauma;

    [Header("Music Fading")]
    public float MusicFadeRate;
    private float musicVolumeTarget = 1f;
    private float musicVolumeVelocity;

    private void Awake() {
        Instance = this;
        transform.position = new Vector3(transform.position.x, transform.position.y, -10f);
    }

    private void Start() {
        ShakeRotator.SetUp(Camera.gameObject);
        ShakeTranslator.SetUp(Camera.gameObject);
    }

    public void ApplyShake(float shake) {
        shakeTrauma = Mathf.Clamp01(Mathf.Max(shakeTrauma, shake));
    }

    [Button("Test Shake")]
    public void TestShake() {
        ApplyShake(TestShakeAmount);
    }
    
    private void Update() {

        ShakeRotator.SetShakeFactor(-1, new ValueFactor(shakeTrauma * ShakeRotateMax, ShakeDecayRate, 10000f), ShakePerlinSpeed);
        ShakeTranslator.SetShakeFactor(-1, new ValueFactor(shakeTrauma * ShakeTranslateMax, ShakeDecayRate, 10000f), ShakePerlinSpeed);
        shakeTrauma = MathUtilities.Decay(shakeTrauma, Time.deltaTime, ShakeDecayRate);
        
        // Music Volume on the Camera Don't Ask
        Music.volume = Mathf.SmoothDamp(Music.volume, musicVolumeTarget, ref musicVolumeVelocity, 1f / MusicFadeRate);

    }

    public void StartMusic() {
        Music.Play();
    }

    public void StopMusic() {
        Music.Stop();
    }

    public void SetMusicTargetVolume(float newTarget) {
        musicVolumeTarget = newTarget;
    }


}
