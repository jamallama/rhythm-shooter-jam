﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Git yer timing-related stuff here
/// </summary>
public class TimingController : MonoBehaviour {

    private bool hasStartedGame;
    
    public int BPM;
    private float BPS => BPM / 60f;

    public static float TimeOfLastReset = 0f;
    public static float GameTime => Time.time - TimeOfLastReset;
    public static float UnscaledTimeOfLastReset = 0f;
    private bool gameIsGoing;

    private void Awake() {
        Time.timeScale = 0f;
    }
    
    void Update() {
        if (Time.unscaledTime > UnscaledTimeOfLastReset + (TimeOfLastReset > 0.1f ? 3f : 6.5f)) {
            hasStartedGame = true;
            Time.timeScale = 1f;
            if (!gameIsGoing) {
                CameraController.Instance.StartMusic();
            }
            gameIsGoing = true;
        }
        else {
            hasStartedGame = false;
            Time.timeScale = 0f;
            if (gameIsGoing) {
                CameraController.Instance.StopMusic();
            }
            gameIsGoing = false;
        }
    }
    
    public float GetWholeNoteTime() {
        return 4f / BPS;
    }

    public float GetHalfNoteTime() {
        return 4f / (2f * BPS);
    }

    public float GetQuarterNoteTime() {
        return 4f / (4f * BPS);
    }

    public float GetEigthNoteTime() {
        return 4f / (8f * BPS);
    }

    public float GetSixteenthNoteTime() {
        return 4f / (16f * BPS);
    }

    public float GetThirtysecondNoteTime() {
        return 4f / (32f * BPS);
    }

}
