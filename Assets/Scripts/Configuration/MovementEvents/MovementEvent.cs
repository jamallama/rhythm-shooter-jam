﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class MovementEvent : ConfigurationEvent {
    public new enum Values {
        Unset = ConfigurationEvent.Values.Unset,
        None = ConfigurationEvent.Values.None,
    }
    
    public Vector2 Destination;
}
