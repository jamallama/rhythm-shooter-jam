﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;
using Random = UnityEngine.Random;

public class BasicMovement : MovementEvent {
    public new enum Values {
        Unset = ConfigurationEvent.Values.Unset,
        None = ConfigurationEvent.Values.None,
        MoveToPoint = 200,
    }
    
    public override void UpdateEvent(string step) {
        Values action = GetValueForString(step);
        switch (action) {
            default:
                Assert.IsTrue(false);
                break;
        }
    }

    // private Transform DetermineTransform(Values moveEvent) {
    //     Transform ret = null;
    //     float enemyToPlayer = (transform.position - playerController.GetPlayerPosition()).magnitude;
    //     List<Transform> sortedWaypoints = GetSortedWaypoints();
    //     switch (moveEvent) {
    //         case Values.NormalMove:
    //             ret = sortedWaypoints[Random.Range(0, 2)];
    //             break;
    //         case Values.MoveTowards:
    //             List<Transform> sortedWayPointsCloserToPlayer = sortedWaypoints.Where(e =>
    //                 (e.position - playerController.GetPlayerPosition()).magnitude <= enemyToPlayer).ToList();
    //             if (sortedWayPointsCloserToPlayer.Count <= 0) {
    //                 // If there are no waypoints left that are closer to the player than we currently are, then just go 
    //                 // to the closest waypoint
    //                 ret = sortedWaypoints[0];
    //             } else {
    //                 // Pick from one of the three closest waypoints that are closer to the player
    //                 ret = sortedWayPointsCloserToPlayer[
    //                     Random.Range(0, Math.Min(2, sortedWayPointsCloserToPlayer.Count))];
    //             }
    //             break;
    //         case Values.MoveAway:
    //             List<Transform> sortedWayPointsFartherToPlayer = sortedWaypoints.Where(e =>
    //                 (e.position - playerController.GetPlayerPosition()).magnitude >= enemyToPlayer).ToList();
    //             if (sortedWayPointsFartherToPlayer.Count <= 0) {
    //                 // If there are no waypoints left that are farther to the player than we currently are, then just go 
    //                 // to the closest waypoint
    //                 ret = sortedWaypoints[0];
    //             } else {
    //                 // Pick from one of the three closest waypoints that are farther to the player
    //                 ret = sortedWayPointsFartherToPlayer[
    //                     Random.Range(0, Math.Min(2, sortedWayPointsFartherToPlayer.Count))];
    //
    //             }
    //             break;
    //         case Values.MoveToMiddle:
    //             ret = middleWaypoints[Random.Range(0, middleWaypoints.Count)];
    //             break;
    //         default:
    //             Assert.IsTrue(false);
    //             break;
    //
    //     }
    //
    //     return ret;
    // }

    public override string[] GetValues() {
        return Enum.GetNames(typeof(Values));
    }

    private Values GetValueForString(string stringName) {
        return (Values)Enum.Parse(typeof(Values), stringName);
    }
}
