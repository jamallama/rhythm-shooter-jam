﻿using UnityEngine;

public abstract class SpawnEvent : ConfigurationEvent {
    public new enum Values {
        Unset = ConfigurationEvent.Values.Unset,
        None = ConfigurationEvent.Values.None,
        Spawn = 11
    }

    public Vector2 SpawnLocation;
    public Quaternion SpawnRotation;

    public Enemy Entity;
}
