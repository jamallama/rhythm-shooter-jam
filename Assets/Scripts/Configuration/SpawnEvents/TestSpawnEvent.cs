﻿using System;
using UnityEngine;

public class TestSpawnEvent : SpawnEvent {
    public new enum Values {
        Unset = SpawnEvent.Values.Unset,
        None = SpawnEvent.Values.None,
        Spawn = SpawnEvent.Values.Spawn
    }

    public override void UpdateEvent(string step) {
        throw new System.NotImplementedException();
    }
    
    public override string[] GetValues() {
        return Enum.GetNames(typeof(TestSpawnEvent.Values));
    }

    private TestSpawnEvent.Values GetValueForString(string stringName) {
        return (TestSpawnEvent.Values)Enum.Parse(typeof(TestSpawnEvent.Values), stringName);
    }
}