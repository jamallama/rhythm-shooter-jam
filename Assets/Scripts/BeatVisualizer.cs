﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BeatVisualizer : MonoBehaviour {

    public RectTransform CoreLineRT;
    public Image CoreLineImage;

    public GameObject SideLineTemplate;

    public float PairLifetime;
    public RectTransform PairStartLeft;
    public RectTransform PairStartRight;

    public List<BeatPackage> Beats = new List<BeatPackage>();

    [Serializable]
    public class BeatPackage {
        public TimingController Controller;

        public float StartTime;
        public float EndTime;
        public float TimeBetweenBeats => Controller.GetQuarterNoteTime();

    }
    
    private Dictionary<BeatPackage, int> beatsStartedByPackage = new Dictionary<BeatPackage, int>();
    
    private List<SideLinePair> pairs = new List<SideLinePair>();
    private class SideLinePair {

        public GameObject LeftObj;
        public RectTransform LeftRT;
        public Image LeftImage;

        public GameObject RightObj;
        public RectTransform RightRT;
        public Image RightImage;
        
        public float BeatTime;
        public float TimeOfCreation;
        public float Age => TimingController.GameTime - TimeOfCreation;
        public float Duration => BeatTime - TimeOfCreation;
        public float PctProgress => Age / Duration;

    }

    [Header("Effects")]
    public AnimationCurve PairLineOpacByProgress;
    public AnimationCurve PairLineScaleByProgress;
    public AnimationCurve CoreLineScaleByTimeSinceLastBeat;
    public Color SuccessColor;
    public Color FailureColor;
    public AnimationCurve ColorStrengthByTimeSinceAction;
    
    private void Start() {
        foreach (BeatPackage bp in Beats) {
            beatsStartedByPackage.Add(bp, 0);
        }
    }

    private void Update() {

        foreach (BeatPackage bp in Beats) {
            if (TimingController.GameTime + PairLifetime > bp.StartTime + bp.TimeBetweenBeats * beatsStartedByPackage[bp] && TimingController.GameTime + PairLifetime < bp.EndTime) {
                GenerateNewPair();
                beatsStartedByPackage[bp]++;
            }
        }

        foreach (SideLinePair pair in pairs) {
            pair.LeftRT.anchoredPosition = Vector2.Lerp(PairStartLeft.anchoredPosition, CoreLineRT.anchoredPosition, pair.PctProgress);
            pair.RightRT.anchoredPosition = Vector2.Lerp(PairStartRight.anchoredPosition, CoreLineRT.anchoredPosition, pair.PctProgress);
            pair.LeftRT.localScale = pair.RightRT.localScale = new Vector2(1f, PairLineScaleByProgress.Evaluate(pair.PctProgress));
            pair.RightImage.color = pair.LeftImage.color = new Color(1f, 1f, 1f, PairLineOpacByProgress.Evaluate(pair.PctProgress));
        }

        CoreLineRT.localScale = Vector2.one * CoreLineScaleByTimeSinceLastBeat.Evaluate(TimingController.GameTime - BeatManager.TimeOfLastBeat);
        CoreLineImage.color = Color.Lerp(Color.white, BeatManager.MostRecentActionSuccess ? SuccessColor : FailureColor, ColorStrengthByTimeSinceAction.Evaluate(TimingController.GameTime - BeatManager.MostRecentActionTime));

        for (int i = pairs.Count - 1; i >= 0; i--) {
            if (pairs[i].PctProgress >= 1) {
                BeatManager.TimeOfLastBeat = pairs[i].BeatTime;
                BeatManager.TimeOfNextBeat = pairs[i + 1].BeatTime;
                BeatManager.PrevBeatConsumed = BeatManager.NextBeatConsumed;
                BeatManager.NextBeatConsumed = false;
                Destroy(pairs[i].LeftObj);
                Destroy(pairs[i].RightObj);
                pairs.RemoveAt(i);
            }
        }
        
    }

    private void GenerateNewPair() {
        SideLinePair newPair = new SideLinePair();
        GameObject leftLine = Instantiate(SideLineTemplate, PairStartLeft.transform.position, Quaternion.identity, transform);
        GameObject rightLine = Instantiate(SideLineTemplate, PairStartRight.transform.position, Quaternion.identity, transform);
        leftLine.SetActive(true);
        rightLine.SetActive(true);
        newPair.LeftObj = leftLine;
        newPair.LeftRT = leftLine.GetComponent<RectTransform>();
        newPair.LeftRT.anchoredPosition = PairStartLeft.anchoredPosition;
        newPair.LeftImage = leftLine.GetComponent <Image>();
        newPair.RightObj = rightLine;
        newPair.RightRT = rightLine.GetComponent<RectTransform>();
        newPair.RightRT.anchoredPosition = PairStartRight.anchoredPosition;
        newPair.RightImage = rightLine.GetComponent <Image>();
        newPair.BeatTime = TimingController.GameTime + PairLifetime;
        newPair.TimeOfCreation = TimingController.GameTime;
        pairs.Add(newPair);
    }
    
}
