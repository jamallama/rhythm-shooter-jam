﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

public class Enemy : MonoBehaviour {
    
    public int healthPoints = 10;
    
    // MOVEMENT
    [Header("Movement")]
    public float MoveSpeed;
    public float ChasePointAcceleration;
    public AnimationCurve ScaleByVelocity;
    public AnimationCurve VelocityMultiplierByDistanceToChasePoint;
    private Vector2 chasePoint;
    private Vector2 chasePointVelocity;
    private Vector2 curDestination;
    public Vector2 TestVector;
    private Vector3 baseScale;

    [Header("Turning")]
    public Transform RotatorTransform;
    public bool RotatesTowardPlayer;
    public float RotationSpeed;

    [Header("Effects")]
    public List<FxController> DamagedFX;
    public GameObject PlayerShotDissolvePrefab;
    
    void Start() {
        curDestination = chasePoint = transform.position;
        baseScale = transform.localScale;
    }

    void Update() {
        if (healthPoints <= 0) {
            Destroy(this.gameObject);
        }

        // Chase Point accelerates toward Destination.
        chasePoint = Vector2.SmoothDamp(chasePoint, curDestination, ref chasePointVelocity, 1f / ChasePointAcceleration);
        
        // Transform Moves toward Chase Point.
        Vector2 diff = chasePoint - (Vector2) transform.position;
        float mag = diff.magnitude;
        float velocity = MoveSpeed * VelocityMultiplierByDistanceToChasePoint.Evaluate(mag);
        transform.Translate(diff.normalized * velocity * Time.deltaTime, Space.World);
        
        // Body Resizes based on Movement.
        transform.localScale = baseScale * ScaleByVelocity.Evaluate(velocity);

        if (RotatesTowardPlayer && MyCharacter.Instance != null) {
            float angles = MathUtilities.RotationToLookAtPoint(-RotatorTransform.up,
                MyCharacter.Instance.transform.position - transform.position);
            RotatorTransform.Rotate(0f, 0f, angles * Time.deltaTime * RotationSpeed);
        }

    }

    [Button("Test Move to Point")]
    public void TestMoveToPoint() {
        MoveToPoint(TestVector);
    }
    
    public void MoveToPoint (Vector2 newDestination) {
        curDestination = newDestination;
    }
    
    private void OnParticleCollision(GameObject other) {
        Debug.Log("Ouch! Oof!");
        Instantiate(PlayerShotDissolvePrefab, transform.position + (other.transform.position - transform.position).normalized * 1.5f, Quaternion.identity, null);
        DamagedFX.ForEach(e => e.TriggerAll(1f));
        healthPoints--;
    }
}
