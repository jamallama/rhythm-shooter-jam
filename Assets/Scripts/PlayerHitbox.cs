﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHitbox : MonoBehaviour {
    public MyCharacter Player;
    private void OnParticleCollision(GameObject other) {
        Player.AdministerTheHurt();
    }
}
