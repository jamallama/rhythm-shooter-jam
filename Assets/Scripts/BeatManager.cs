﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

public class BeatManager : MonoBehaviour {

    public static float TimeOfLastBeat = -100f;
    public static float TimeOfNextBeat;
    public static bool PrevBeatConsumed;
    public static bool NextBeatConsumed;

    public const float BeatToleranceBefore = 0.14f;
    public const float BeatToleranceAfter = 0.09f;

    public static float MostRecentActionTime = -100f;
    public static bool MostRecentActionSuccess = true;
    
    public static bool TryBeatAction(bool consumesBeat) {
        bool success = false;
        if (!PrevBeatConsumed && TimingController.GameTime > TimeOfLastBeat - BeatToleranceBefore && TimingController.GameTime < TimeOfLastBeat + BeatToleranceAfter) {
            success = true;
            if (consumesBeat) {
                PrevBeatConsumed = true;
                MostRecentActionTime = TimingController.GameTime;
                MostRecentActionSuccess = true;
            }
        } else if (!NextBeatConsumed && TimingController.GameTime > TimeOfNextBeat - BeatToleranceBefore && TimingController.GameTime < TimeOfNextBeat + BeatToleranceAfter) {
            success = true;
            if (consumesBeat) {
                NextBeatConsumed = true;
                MostRecentActionTime = TimingController.GameTime;
                MostRecentActionSuccess = true;
            }
        }
        
        if (success) {
            Debug.Log("<color=green>ON!</color>");
            if (consumesBeat) {
                MostRecentActionTime = TimingController.GameTime;
                MostRecentActionSuccess = true;
            }
            return true;
        }
        else {
            float timeToNextBeat = TimingController.GameTime - TimeOfNextBeat;
            float timeSinceLastBeat = TimingController.GameTime - TimeOfLastBeat;
            float offset = Mathf.Abs(timeToNextBeat) < Mathf.Abs(timeSinceLastBeat)
                ? timeToNextBeat
                : timeSinceLastBeat;
            Debug.Log("<color=red>OFF!</color> " + offset.ToString("F2"));
            MostRecentActionTime = TimingController.GameTime;
            MostRecentActionSuccess = false;
            return false;
        }
    }

}
